package br.com.itau;

public class ContaCorrente {
    private Cliente cliente;
    private Boolean deposito;
    private Boolean saque;
    private Double valor;
    private Double saldo;

    public ContaCorrente(){}

    public ContaCorrente(Cliente cliente, Boolean saque, Boolean deposito, Double valor, Double saldo){
        this.cliente    = cliente;
        this.saque      = saque;
        this.deposito   = deposito;
        this.valor      = valor;
        this.saldo      = acaoContaCorrente(saldo);
    }

    public Double acaoContaCorrente(Double saldo){
        if(saque){
            this.saldo -= valor;
        }
        else{
            if(deposito){
                this.saldo += valor;
            }
        }
        return this.saldo;
    }


    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Boolean getDeposito() {
        return deposito;
    }

    public void setDeposito(Boolean deposito) {
        this.deposito = deposito;
    }

    public Boolean getSaque() {
        return saque;
    }

    public void setSaque(Boolean saque) {
        this.saque = saque;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
