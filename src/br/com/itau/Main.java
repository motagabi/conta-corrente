package br.com.itau;

import java.util.Map;

public class Main {
    public static void main(String[] args) {
        IO.imprimirMensagemInicial();
        Map<String, String> dadosCliente =   IO.solicitarDadosCliente();
        Integer idade = Integer.parseInt(dadosCliente.get("idade"));

        Cliente clienteIdade = new Cliente(idade);
        if(!clienteIdade.verificaIdade()){
            System.out.println("********************************");
            System.out.println("***PERMITIDO APENAS PARA MAIORES");
            System.out.println("***IDADE INFORMADA:"+idade);
            System.out.println("********************************");
        }else {
            //Instancia objeto de cliente
            Cliente cliente = new Cliente(
                    dadosCliente.get("nome"),
                    dadosCliente.get("cpf"),
                    idade
            );
            System.out.println("********************************");
            System.out.println("Nome: "+cliente.getNome());
            System.out.println("CPF : "+cliente.getCpf());
            System.out.println("Idade: "+cliente.getIdade());
            System.out.println("Saldo inicial: "+dadosCliente.get("saldo"));

            //sacar
            ContaCorrente contaCorrenteSacar = new ContaCorrente();
            contaCorrenteSacar.setCliente(cliente);
            contaCorrenteSacar.setSaque(true);
            contaCorrenteSacar.setValor(50.0);
            System.out.println("R$ Saque: "+contaCorrenteSacar.getValor());

            //depositar
            ContaCorrente contaCorrenteDepositar = new ContaCorrente();
            contaCorrenteDepositar.setCliente(cliente);
            contaCorrenteSacar.setDeposito(true);
            contaCorrenteDepositar.setValor(100.0);
            System.out.println("R$ Deposito: "+contaCorrenteDepositar.getValor());

            System.out.println("Saldo Final: "+dadosCliente.get("saldo"));

        }
    }
}

