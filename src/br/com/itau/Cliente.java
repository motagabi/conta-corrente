package br.com.itau;

public class Cliente {
    private String cpf;
    private String nome;
    private int idade;

    public Cliente() {

    }
    public Cliente(int idade) {
        this.idade  = idade;
    }

    public Cliente (String cpf, String nome, int idade){
        this.cpf    = cpf;
        this.nome   = nome;
        this.idade  = idade;
    }

    public Cliente(String nome, String cpf, int idade, double saldo) {
    }

    public boolean verificaIdade(){
        if(idade >=18){
            return true;
        }
        return false;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }
}

