package br.com.itau;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Map;


public class IO {

    public static void imprimirMensagemInicial(){
        System.out.println("********************************");
        System.out.println("***Bem-vindo ao sistema Banco***");
        System.out.println("********************************");

    }
    //simulação - receber dados de console -
    public static Map<String, String> solicitarDadosCliente(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite o nome: ");
        String nome =   scanner.nextLine();
        System.out.println("Digite o CPF:   ");
        String cpf =   scanner.nextLine();
        System.out.println("Digite a Idade: ");
        String idade =   scanner.nextLine();
        System.out.println("Deposito inicial: ");
        String saldo =   scanner.nextLine();

        Map<String, String> dados = new HashMap<>();
        dados.put("nome", nome);
        dados.put("cpf", cpf);
        dados.put("idade", idade);
        dados.put("saldo", saldo);

        return dados;

    }

}
